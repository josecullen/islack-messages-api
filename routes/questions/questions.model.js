var mongoose = require('mongoose')
var Schema = mongoose.Schema

var questionSchema = {
  question: {type : String, required : true}
}

module.exports = mongoose.model('Question', questionSchema)
